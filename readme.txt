A short description of the project : An Android 3D game with singleplayer and multiplayer mode with 2 players involved.
Spartans will fight with spiders in the arena to survive and to earn score and who got higher score will be winner.

Instructions on how to check out and build the project:
Step 1: Make a account on Bitbucket
Step 2: Install Sourcetree
Step 3: Clone the repo with Sourcetree
Step 4: Fetch the project
Step 5: Pull it
Step 6: Open the project with Unity3D 4.6
Step 7: Choose File -> Build 
Step 8: Choose Android platform 
Step 9: Choose approriate SDK folder
Step 10: May need to modify some Android target build (optional)
Step 11: Build an APK file
Step 12: Copy the file to your Android phone
Step 13: Install it and run on your phone

NOTE: the APK file is inside the repo, copy that APK file to the phone directory then click that APK file and run.

-----------------------------------------------------------------------------------------

Unit Testing:

+ Movement Testing
-Description: to test if after the player move buttons, the new position of player will be
corresponding to the old one plus aprroriate diretions X or Z
-Located at HellRain/Assets/Editor/MovementTest.cs


References :

Sound Effects: 
- all sound effects from www.freefx.co.uk:
	+ http://www.freesfx.co.uk/download/?type=mp3&id=1048
        + http://www.freesfx.co.uk/download/?type=mp3&id=5169&eula=true
- music background :
	+ http://incompetech.com/music/royalty-free/index.html?feels%5B%5D=Action&feels%5B%5D=Aggressive&feels%5B%5D=Epic


3D Models: 
	+ Arena : https://www.assetstore.unity3d.com/en/#!/content/1715
	+ Player : https://www.assetstore.unity3d.com/en/#!/content/1735
	+ Enemy : https://www.assetstore.unity3d.com/en/#!/content/11869
